<?php namespace JonathanHindi\Api;

use Symfony\Component\HttpFoundation as HttpFoundation;

class Api
{
    /**
     * Laravel application
     * 
     * @var Illuminate\Foundation\Application
     */
    public $app;

    /**
     * Create a new Api instance.
     * 
     * @return void
     */
    public function __construct()
    {
        $this->app = app();
    }

    /**
     * Returns the Laravel application
     * 
     * @return Illuminate\Foundation\Application
     */
    public function app()
    {
        return $this->app;
    }

    /**
     * Returns json response in a standardized format
     *
     * @param int Http Status Code
     * @param string Message
     * @param mixed Data to return in the json data property
     * @return json object
     */
    public function response($code, $message = '', $data = NULL, array $headers = array())
    {   

        // If no message provided, Provide normal Response messages from Symfony\Component\HttpFoundation
        if( $message == '' ){
            $message = isset(HttpFoundation\Response::$statusTexts[$code]) ? HttpFoundation\Response::$statusTexts[$code] : '';
        }  

        $data = $this->isArray($data);

        return $this->app['Response']
            ->json([
                'error'     => $this->isError($code),
                'code'      => $code,
                'message'   => $message,
                'data'      => !is_null($data) ? json_decode($data) : NULL,
            ], $code, $headers);

    }

    /**
     * Helper function to change tell if http code is error 
     * {@link [http://httpstatus.es/]}
     * 
     * @param int $code http code
     * @return bool 
     */
    private function isError($code)
    {

        // if not within range set as 500
        if($code < 200 || $code > 599) $code = 500;

        // if between 200 and 226 set status to false
        if($code >= 200 && $code <= 226) $status = false;

        // if between 400 and 599 set status to true
        if($code >= 400 && $code <= 599) $status = true;

        // return status
        return $status;

    }

    /**
     * if isArray convert to json
     *
     * @param mixed param
     * @return mixed Return json representation if array or return the same if not array.
     */
    private function isArray($param)
    {
        if( is_array($param) ){
            return json_encode($param);
        }

        return $param;
    }

    /**
     * HanldeImage Moving and returning path for ImageagbleType to save in database.
     *
     * @param file
     * @param EntityType
     * @param ImageableType
     * @return $path
     */
    public function handleImage(&$file, $EntityTitle, $imageableType){
                    
        // Get Extension
        $originalExtension = $file->getClientOriginalExtension();

        // Generate Path
        $path = $this->app['config']->get('api.images_dir') . '/' . $this->app['Str']->lower($imageableType);

        // Move file to Images Directory
        $path = $file->move(public_path() . $path, $this->app['Str']->slug($EntityTitle) . '_' . md5(date('Y-m-d H:i:s:u')) .'.' . $originalExtension);

        // Remove public_path() to save relative directory only in database
        $path = str_replace(public_path(), '', $path);

        return $path;
    }

}